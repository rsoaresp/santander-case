#!/bin/bash

model_id=$1

path=$(find "$(pwd)" -name $model_id)
docker run --rm \
-p 8000:5000 \
-v "$path":/opt/mlflow/ \
--entrypoint="mlflow" \
santander_mlflow models serve -m artifacts/model -h 0.0.0.0 -p 5000