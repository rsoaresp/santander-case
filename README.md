# Santander dataset

This is a dataset from Santander where we have a binary classification depending upon 200 numerical features. As the binary target indicates if the client will perform a transaction in the future, let's, for the sake of a more illustrative description, think of it as a purchase. It gives our problem a clean meaning, to classify if a client will buy or not an item.

If we can classify whether a client is a potential buyer, we can direct our marketing efforts to them. This will save the company money, and we'll not bother clients that aren't interested. 

This interpretation will also affect the metric's names and meaning that we'll use during the curse of this problem.

## Exploratoy environment

The first thing we have to do is to create an exploratory environment to play with the dataset and start modeling. 

Our exploratory environment has to be reproducible and our experiments have to be trackable. We want to know precisely what's working and what's not. In order to achieve these requirements, we have packed two docker images, one with a jupyter lab with the most common scientific libraries and another one with [mlflow](https://mlflow.org/), that will help us track experiments (and also put them in production, later).

In order to build the images, run the following [docker-compose](https://docs.docker.com/compose/)

```bash
docker-compose build
```

and run it with

```bash
docker-compose up
```

During the loading of the containers, you will see the corresponding links to start the services in your browser. They typically looks like `http://127.0.0.1:10000/lab?token=<token>` for the jupyter lab and `http://0.0.0.0:5000` for mlflow.

## Running experiments

The exploratory analysis consists of the notebook `eda`, which performs basic exploratory data analysis, in order to allow us to grasp a basic understanding of the dataset. Once we are familiarised with the dataset, we can start modeling it, which is done on the `model_experiment` folder, containing some notebooks with sklearn's algorithms. Since it's a binary
classification, our first guess is to perform a fit of a logistic regressor (after working with the dataset inbalance).

### Inbalanced dataset

One of the complications of this dataset is that class `1` is 9 times less present than class `0`. This, for instance, pushes us to consider algorithms to deal with this imbalance and forbids the use of simple metrics such as accuracy. One useful tool to study our problem is through the [confusion matrix](https://en.wikipedia.org/wiki/Confusion_matrix). From its entries we can calculated without complication information such as true positive(negative) rates for both classes, which in this case, is a metric that reflects better how well our model is performing. All of this metrics translated directly into business, because, if we think on the ilustrative case of user's buying or not an item, the true positive of class 1 indicates the willing clients that we reach out, whereas its false positive are clients that never wanted to buy the item but which received iteraction from us and probably were upset by this.

The notebooks with logistic regression exploration are ready to be executed and we can play with the parameters. All the executions are logged into mlflows and we can test any of the models produced, as explained in the next section. 

## Local serve 

After playing with the notebooks, we have a list of models stored that we can test locally how it behaves when encountering new data. We can look at the mlflow user interface to select
models that we want to test and serve them locally for further testing. Once you have the corresponding id, from the list of tracked models, create its api with the command,

```bash
./local-serve.sh model-id
```

that will serve the model on the localhost in the specifices `port`.


### Invoking the local endpoint

In order to test locally if the model is working as expected and if they behave correctly for each kind of payload, we can test it using Python's request library.

```python
# Create random data just to see that the endpoint is working.
columns=[f'var_{i}' for i in range(0, 200)]
random_data = pd.DataFrame(np.random.random((1, 200)), columns=columns)

# Make the request. We use to_json in order to convert the dataframe into the expected data format.
r = requests.post("http://0.0.0.0:8000/invocations", headers={"Content-Type": "application/json"}, data=random_data.to_json(orient='split'))
```

Note that this assumes that we picked port 8000 to serve.

## Production

In this section we'll talk about how to put our model into production. We picked a particular case and wwe will take advantage of the integration between Mlflow and 
Sagemaker to serve our endpoint.

### Serve an endpoint with Sagemaker

```bash
./sagemaker-serve.sh model-id endpoint-name sagemaker-execution-role instance-type instance-count region-name
```

Don't forget that it's necessary to have aws [credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) configured on your local machine.

### Invoking Sagemaker Endpoint

```python
import boto3
import numpy as np
import pandas as pd

session = boto3.session.Session(profile_name='my-profile-name', region_name='us-east-1')
client = boto3.client('sagemaker-runtime')

# Create a random data, just to test if it's working
columns=[f'var_{i}' for i in range(0, 200)]
test_data = pd.DataFrame(np.random.random((1, 200)), columns=columns)

response = client.invoke_endpoint(
    EndpointName='mlflow-teste',
    ContentType="application/json",
    Body=random_data.to_json(orient='split'
    )

print(response.status_code)
```
