#!/bin/bash

model=$1
app_name=$2
role=$3
image=$4
instance_type=$5
instance_count=$6
region=$7

path=$(find "$(pwd)" -name $model)

conda create -y -n push-env python=3.9 
conda config --add channels conda-forge
conda config --set channel_priority strict

conda install -y -n push-env mlflow boto3
conda run -n push-env mlflow sagemaker build-and-push-container
conda run -n push-env mlflow sagemaker deploy --app-name $app_name --model-uri $path --execution-role-arn $role --image-url $image --region-name $region --instance-count $instance_count --instance-type $instance_type
